# Lugar seguro

Cuando te diagnostican un cáncer puede ser difícil desenredar tus emociones, y aún más difícil comunicarlas a los que te rodean.

Esta aplicación te ayuda a expresarte de formas que van más allá de las palabras y a compartir esas emociones con un círculo de apoyo, de la forma que tú elijas. Ya sea un padre, un amigo, un cónyuge, un compañero de trabajo o un hijo, la aplicación cuenta con indicaciones y recursos para que puedas comunicar y registrar cómo te sientes de una forma intuitiva y menos estresante.

***English***

When you are diagnosed with cancer it can be hard to untangle your emotions, and harder still to communicate these to those around you.

This app helps you express yourself in ways beyond words and share those emotions with a support circle, in the way you choose. Be it a parent, friend, spouse, work colleague or child the app has prompts and resources, so you are able to communicate and record how you are feeling in an intuitive and less stressful way.

## Aplicación web (Web app)

El código de la aplicación web puede encontrarse [aquí](./website/README.md)

Code for the webapp can be found [here](./website/README.md)

## Documentos (Documents)

[Información técnica - Technical information (Spanish)](./documents/ficha_de_proyecto.pdf)

[Diapositivas - Slide deck](./documents/slide_deck.pdf)

[Vídeo del prototipo - Video of prototype](./documents/SPOILER_app-prototype.mov)

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="./documents/SPOILER_app-prototype.mov" type="video/mov">
  </video>
</figure>

- [Referencias](referencias.jpg)
- [Deseos](deseos.jpg)
- [user_types](user_types.jpg)
- [roadmap_of_features](roadmap_of_features.jpg)
- [name_brainstorming](name_brainstorming.jpg)
- [sketches](sketches.jpg)